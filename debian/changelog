ngetty (1.1-13) unstable; urgency=medium

  * QA upload
  * Assume users installing custom gettys also want login
    (Closes: #1074102)

 -- Chris Hofstaedtler <zeha@debian.org>  Sun, 23 Jun 2024 11:29:12 +0200

ngetty (1.1-12) unstable; urgency=medium

  * QA upload
  * Install into /usr/sbin instead of /sbin

 -- Chris Hofstaedtler <zeha@debian.org>  Mon, 27 Nov 2023 03:33:39 +0100

ngetty (1.1-11) unstable; urgency=medium

  * QA upload
  * Drop unnecessary salsa-ci.yml
  * Build with default libc
  * Fix lintian: depends-on-obsolete-package

 -- Bastian Germann <bage@debian.org>  Mon, 24 Jul 2023 11:28:39 +0200

ngetty (1.1-10) unstable; urgency=medium

  * QA upload.

  [ Joao Eriberto Mota Filho ]
  * debian/control: bumped Standards-Version to 4.6.0.
  * debian/copyright: updated packaging copyright data.
  * debian/tests/control: removed a test using ampersand to run in background.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends-Arch: Drop versioned constraint on dietlibc-dev.
    + ngetty: Drop versioned constraint on lsb-base in Depends.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 17 Mar 2022 14:52:40 -0300

ngetty (1.1-9) unstable; urgency=medium

  * QA upload.
  * debian/rules: added a 'echo' line to ignore false positives from blhc.
    (Closes: #962990)
  * debian/salsa-ci.yml: removed no longer needed instruction to allow failures
    from blhc.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 03 Aug 2020 00:05:45 -0300

ngetty (1.1-8) unstable; urgency=medium

  * QA upload.

  [ Sophie Brun ]
  * Add needs-root restriction to second autopkgtest too as it was failing
    with the qemu backend in kali's autopkgtest.

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 25 Jun 2020 08:36:49 +0200

ngetty (1.1-7) unstable; urgency=medium

  * QA upload. (Closes: #904067)
  * debian/control:
      - Added ${shlibs:Depends} variable to Depends field.
      - Added VCS fields.
  * debian/dirs: useless. Removed.
  * debian/docs: removed CHANGES because dh_installchangelogs already detect it.
  * debian/patches/:
      - Added a numeric prefix to all patches.
      - Added patch 50_fix-gcc-hardening.patch to enable the GCC hardening.
  * debian/rules:
      - Added DEB_BUILD_MAINT_OPTIONS to improve the GCC hardening.
      - Removed no longer needed variable DH_OPTIONS.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Wed, 17 Jun 2020 06:42:25 -0300

ngetty (1.1-6) unstable; urgency=medium

  * QA upload.
  * debian/control: the Homepage field removed, address unavailable.
  * debian/copyright: migrated to Machine-readable 1.0 format.
  * debian/patches/010_fix-spelling-man.patch: added to fix a misspelling
    in manpage.
  * debian/rules: removed some trash and useless lines.
  * debian/tests/control: added restrictions to running correctly.
    (Closes: #961455)
  * debian/watch: created using a fake site to explain about the
    current status of the original upstream homepage.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Sun, 31 May 2020 08:25:31 -0300

ngetty (1.1-5) unstable; urgency=medium

  * QA upload.
  * debian/tests/control: created to perform trivial CI tests.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Fri, 15 May 2020 13:44:14 -0300

ngetty (1.1-4) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #911035)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control: bumped Standards-Version to 4.5.0.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Mon, 11 May 2020 19:01:59 -0300

ngetty (1.1-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild using a version of dietlibc that does not use vsyscall
    (Closes: #802185)
  * Add Built-Using relationship to dietlibc (Closes: #847579)
  * sig_action.h: Define _NSIG_WORDS if necessary (Closes: #829550)
  * ngetty-helper: Relax permissions check on Conf (Closes: #623264)
  * Remove explicit use of quilt, which is not needed in a 3.0 (quilt)
    format package
  * Move dietlibc from Build-Depends to Build-Depends-Arch, as is not
    needed when building the source package
  * Use debhelper compat level 10
    - Upstream changelog is only installed as changelog.gz, not also as
      CHANGES
  * debian/control: Update Standards-Version to 4.1.5
    - Set Rules-Requires-Root: no

 -- Ben Hutchings <ben@decadent.org.uk>  Thu, 19 Jul 2018 04:19:18 +0100

ngetty (1.1-3) unstable; urgency=low

  * debian/ngetty.lintian-overrides: Remove dot-slash for newer lintian.
  * debian/ngetty.init.sample: Rename (was: ngetty.init).
    It's just a sample now, it's better to change inittab in most cases.
  * debian/ngetty.examples: New.
  * debian/README.Debian: Add explanation (Closes: #614049).

 -- NIIBE Yutaka <gniibe@fsij.org>  Sat, 24 Nov 2012 16:27:43 +0900

ngetty (1.1-2) unstable; urgency=low

  * debian/control (Depends): Add lsb-base.
  * debian/ngetty.init: New (Closes: #470802, #495765).
    Not use inittab.
    Thanks to Sheridan Hutchinson and Radovan Garabík.

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 16 Feb 2011 14:18:47 +0900

ngetty (1.1-1) unstable; urgency=low

  * New upstream release.
  * debian/ngetty.lintian-overrides: Renamed from lintian.overrides.
  * debian/rules: Use dh.
  * debian/ngetty.manpages: New.
  * debian/source/format: New.
  * debian/control (Build-Depends): Require newer debhelper and quilt.
    (Standards-Version): Conform to 3.9.1.
    (Description): Mention the binary package is compiled with dietlibc.
    (Closes: #600991).  Thanks to Marco Bodrato.
  * debian/compat: Updated.
  * debian/patches/01_no_gz_manual_install.diff: New.

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 15 Feb 2011 17:31:28 +0900

ngetty (1.0-1) unstable; urgency=low

  * New upstream release (Closes: #532558).

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 09 Feb 2010 11:52:11 +0900

ngetty (0.4-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Standards-Version): Conform to 3.8.0.

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 01 Jul 2008 14:05:10 +0900

ngetty (0.3-3) unstable; urgency=low

  * debian/rules, debian/overrides, debian/control: Build with dietlibc.
    Patch from Julian Gilbey.  Closes: #470946.
  * CHANGES, Makefile, README, Version, all_defs.h, check_first.c,
    cleanutmp.c,do_utmp.c, get_headers, get_uptime.c, init.d/ngetty,
    lib.h, logname_isprint.h, ngetty-argv.c, ngetty-helper.c, ngetty.8,
    opts_do.c, sample.Conf, scan_number.h, scan_ulong.c, str_defs.h,
    test-helper.c, tryboottime.c, trysysinfo.c, tzmap.c, utmp_do.c,
    utmp_io.c, x_atoi.c: Incorporate upstream changes.

 -- NIIBE Yutaka <gniibe@fsij.org>  Sun, 06 Apr 2008 15:25:33 +0900

ngetty (0.3-2) unstable; urgency=low

  * Incorporate upstream change of nwho.c to fix FTBFS on amd64.

 -- NIIBE Yutaka <gniibe@fsij.org>  Sun, 02 Mar 2008 21:45:55 +0900

ngetty (0.3-1) unstable; urgency=low

  * Initial release (Closes: #461663).

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 13 Feb 2008 10:13:00 +0900

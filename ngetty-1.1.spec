Summary: getty replacement - one single daemon for all consoles
Name: ngetty
Version: 1.1
Release: 1
Group: System Environment/Daemons
Packager: Blair Lowe <comedownhere@compeng.net>
Source: http://riemann.fmi.uni-sofia.bg/ngetty/ngetty-%{version}.tar.gz
URL: http://riemann.fmi.uni-sofia.bg/ngetty/
License: GPL
Prefix: /
BuildRoot: %{_tmppath}/%{name}-%{version}-root

%description

Ngetty is a daemon that starts login sessions on virtual console
terminals, on demand.  It is a good replacement for all those getty
processes started from init that, most of the time, are only taking up
memory.  When compiled statically with dietlibc, the ngetty binary
size is only about 2k and uses considerably less memory than a getty.

You have to edit /etc/inittab to make ngetty default.  See the
manual page or the examples on ngetty home page.

%prep

%setup -q

%build
make CC='diet -Os gcc -W' prefix=${RPM_BUILD_ROOT}

%install
make install install_other prefix=${RPM_BUILD_ROOT}

%clean
rm -rf ${RPM_BUILD_ROOT}

%post
( cd /etc/ngetty && test -x setup && ./setup ) || true

%postun

%files
%defattr(-,root,root)
/sbin
/usr
/etc
%doc README

%changelog
* Thu Apr 10 2008 Nikola Vladov <v20-ngrpm@riemann.fmi.uni-sofia.bg>
- remove /usr prefix
- add option for dietlibc

* Fri Mar 12 2008 Blair Lowe <comedownhere@compeng.net>
- Create rpm for Nikola Vladov

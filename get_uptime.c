#include "sysinfo_defs.h"
#include "boottime_defs.h"

#include <time.h>
#include <sys/types.h>

#ifdef HAVE_SYSTEM_SYSINFO
#include <sys/sysinfo.h>

int get_uptime(time_t *uptime) /*EXTRACT_INCL*/{
  struct sysinfo info;
  if (sysinfo(&info)) return -1;
  *uptime = info.uptime;
  return 0;
}

#else

#ifdef HAVE_SYSTEM_BOOTTIME
#include <sys/sysctl.h>
#include <sys/time.h>

int get_uptime(time_t *uptime) { 
  struct timeval res = { 0, 0 };
  int req[2] = { CTL_KERN, KERN_BOOTTIME };
  size_t res_len = sizeof res;
  if (sysctl(req, 2, &res, &res_len, 0, 0) >= 0 && res.tv_sec > 0) {
    *uptime = res.tv_sec;
    return 0;
  } 
  return -1;
}
#endif
#endif

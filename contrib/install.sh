#!/bin/sh -x
export PATH=/bin:/usr/bin

if [ "$DESTDIR" = "" ] ; then
    P="-o root -g root"   
fi

prefix="$DESTDIR"
mandir="$prefix/usr/share/man"
man8dir="$mandir/man8"

sbin_prefix="$prefix/sbin"
etc_prefix="$prefix/etc"
ngetty_prefix="$etc_prefix/ngetty"
usrbin_prefix="$prefix/usr/bin"

### ----- stop editing here -----
if test ! -f ./etc/ngetty/Conf -o ! -x ./sbin/ngetty ; then
    echo go out
    exit 1
fi

umask 022

mk_dir () {
  test -d $2 || install -m $1 $P -d $2
}

mk_dir 755 $usrbin_prefix
mk_dir 755 $sbin_prefix
mk_dir 755 $man8dir
mk_dir 755 $etc_prefix
install -m 700 $P -d $ngetty_prefix

install -m 755 $P ./usr/bin/* $usrbin_prefix
install -m 2711 -o root -g utmp ./usr/bin/cleanutmp $usrbin_prefix || \
  install -m 755 $P ./usr/bin/cleanutmp $usrbin_prefix

install -m 755 $P ./sbin/* $sbin_prefix
install -m 644 $P ./usr/share/man/man8/* $man8dir

install -m 755 $P ./etc/ngetty/setup $ngetty_prefix
install -m 644 $P ./etc/ngetty/Version $ngetty_prefix
install -m 600 $P ./etc/ngetty/sample.Conf $ngetty_prefix
test -f $ngetty_prefix/Conf || \
  install -m 600 $P ./etc/ngetty/Conf $ngetty_prefix

cd /etc/ngetty && test -x setup && ./setup

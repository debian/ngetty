#include <unistd.h>
#include <fcntl.h>
#include "lib.h"

unsigned long utmp_users() /*EXTRACT_INCL*/ {
  unsigned long u=0;
#ifdef USE_LIBC_UTMP
  struct utmp_type *ut;
  f_setutent();
#else
  struct utmp_type ut[1];
  int fd = open(Utmp_File, O_RDONLY);
#endif
  while (f_getutent())
    if (ut->ut_type == USER_PROCESS) u++;
  f_endutent();
  return u;
}

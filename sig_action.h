#include <signal.h>

#ifdef NGETTY_TINY
#if defined(__linux__) && defined(__dietlibc__)

#ifdef NGETTY_i386
extern int system__errno;
#define errno system__errno
#endif

#if defined(__i386__) || defined(__x86_64__)

#define sigemptyset(set) \
do { \
  set->sig[0]=0; \
  if (_NSIG_WORDS>1) set->sig[1]=0; \
  if (_NSIG_WORDS>2) { \
    set->sig[2]=0; \
    set->sig[3]=0; \
  } \
} while(0)

int __rt_sigaction(int signum, const struct sigaction *act, struct sigaction *oldact, long nr);

#define sigaction(signum, act, oldact) \
   __rt_sigaction(signum, act, oldact, _NSIG/8)

#endif
#endif
#endif
